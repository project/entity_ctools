
### Contents
  
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers

### Introduction
This module adds a [Ctools][ctools] contexts plugin for entity forms
(add and edit forms). Currently tested for core entities (node, user, taxonomy
term, taxonomy vocabulary) and for entities created by [Entity
Construction Kit (ECK)][eck]

### Requirements
[Chaos tool suite (ctools)][ctools]  
[Entity API][entity]  

### Recommended modules
[Panels][panels]  
[Entity Construction Kit (ECK)][eck]  

### Installation
* Install as you would normally install a contributed Drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

### Configuration
The module has no menu or modifiable settings. There is no configuration. When
enabled, there will be new contexts available for e.g. pages created by
[Panels][panels] and Page manager ([Ctools][ctools]).

#### Maintainer
 * [Braindrift][profile]


[ctools]: https://www.drupal.org/project/ctools
[entity]: https://www.drupal.org/project/entity
[panels]: https://www.drupal.org/project/panels
[eck]: https://www.drupal.org/project/eck
[profile]: https://www.drupal.org/user/418444
