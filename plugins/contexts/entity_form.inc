<?php
/**
 * @file
 * Plugin to provide an entity_form context.
 */

$plugin = array(
  'title' => t('Entity form'),
  'description' => t('An entity form.'),
  'context' => 'entity_ctools_entity_form_context_create',
  'edit form' => 'entity_ctools_entity_form_context_settings_form',
  'defaults' => array(
    'type' => '',
    'entity_type' => '',
    'entity_id' => '',
    'action' => 'add',
    'redirect' => FALSE,
  ),
  'keyword' => 'entity_form',
  'context name' => 'entity_form',
  'convert list' => array(
    'type' => t('Entity type'),
    'bundle' => t('Entity bundle'),
  ),
  'convert' => 'entity_ctools_entity_form_context_convert',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the entity type for this context.'),
  ),
  'get child' => 'entity_ctools_entity_form_context_get_child',
  'get children' => 'entity_ctools_entity_form_context_get_children',
);

/**
 * Ctools contexts callback: get child.
 */
function entity_ctools_entity_form_context_get_child($plugin, $parent, $child) {
  $plugins = entity_ctools_entity_form_context_get_children($plugin, $parent);
  return $plugins[$parent . ':' . $child];
}

/**
 * Ctools contexts callback: get children.
 */
function entity_ctools_entity_form_context_get_children($plugin, $parent) {
  return entity_ctools_entity_form_get_children($plugin, $parent);
}

/**
 * Ctools contexts callback: context.
 */
function entity_ctools_entity_form_context_create($empty, $data = NULL, $conf = FALSE, $plugin = array()) {
  $cache = &drupal_static(__FUNCTION__, array());
  $cid = array();

  if (is_object($data)) {

    $info = array();
    $info['name'] = $plugin['name'];
    $info['action'] = 'edit';
    $info['entity_id'] = entity_id($plugin['entity type'], $data);

  }
  elseif (is_array($data)) {

    $info = $data;

  }

  if (isset($info)) {
    $cid[] = $info['name'];
    $cid[] = $info['action'];
    $cid[] = $info['action'] == 'add' ? $info['type'] : $info['entity_id'];
  }
  else {
    $info = NULL;
    $cid[] = $plugin['name'];
    $cid[] = 'empty';
    $cid[] = 'none';
  }

  $cid = implode(':', $cid);

  if (!isset($cache[$cid])) {

    $cache[$cid] = _entity_ctools_entity_form_context_create($empty, $info, $conf, $plugin);

  }

  return $cache[$cid];
}

/**
 * Helper function form entity_ctools_entity_form_context_create().
 */
function _entity_ctools_entity_form_context_create($empty, $data = NULL, $conf = FALSE, $plugin = array()) {
  $entity_type = $plugin['entity type'];

  $action = isset($data['action']) ? $data['action'] : 'add';

  $types = array(
    'form',
    $entity_type . '_' . $action,
    $entity_type . '_form',
    $entity_type . '_' . $action . '_form',
    // This gives access to entity form fields.
    $entity_type,
    'entity_form',
    "entity:{$entity_type}",
  );
  $context = new ctools_context($types);
  $context->plugin = $plugin['name'];
  $context->keyword = $plugin['keyword'];

  if ($empty) {
    return $context;
  }

  if (!empty($data)) {

    $entity_info = entity_get_info($entity_type);
    $entity_keys = $entity_info['entity keys'];

    if ($action == 'edit') {
      $op = 'update';
      $entity = entity_ctools_entity_form_context_get_entity($entity_type, $data['entity_id']);
    }
    else {

      $op = 'create';
      list(, $bundle) = explode(':', $data['type']);
      $entity_values = array(
        $entity_keys['bundle'] => $bundle,
        'language' => LANGUAGE_NONE,
      );

      if (isset($plugin['form_info']['entity_values'])) {
        $entity_values = $plugin['form_info']['entity_values'] + $entity_values;
      }

      $entity = entity_create($entity_type, $entity_values);

    }

    if (entity_access($op, $entity_type, $entity)) {

      if ($conf && $conf['redirect']) {
        $data['redirect'] = $conf['redirect'];
      }

      $entity->entity_form_context_data = $data;

      $form = array();
      $form_state = array(
        'build_info' => array(
          'args' => array($entity),
        ),
      );

      if (isset($plugin['form_info'])) {

        $form_id = $plugin['form_info']['form_id'];
        $form = drupal_build_form($form_id, $form_state);

      }
      else {

        $form_callback = isset($entity_info['form callback']) ? $entity_info['form callback'] : '';

        if (strpos($form_callback, 'entity_metadata_form') === 0) {

          if ($action == 'add') {

            switch ($entity_type) {
              case 'taxonomy_term':
                $entity->vid = isset($entity->vid) ? $entity->vid : taxonomy_vocabulary_machine_name_load($bundle)->vid;
                break;
            }

          }

          $form = $form_callback($entity);
          $form_id = $form['#form_id'];

        }
        else {

          $form_id = $form_callback;
          $form = drupal_build_form($form_id, $form_state);

        }

      }

      $context->data = $entity;

      if ($action == 'edit') {
        $context->argument = entity_id($entity_type, $entity);
      }
      else {
        $context->argument = $data['type'];
      }

      $context->form = $form;
      $context->form_id = $form_id;
      $context->form_title = '';

      return $context;
    }

  }

}

/**
 * Helper function to load entity by its uuid.
 *
 * @param string $entity_type
 *    The entity type.
 * @param mixed $entity_id
 *    Can be either a numeric id or a uuid.
 *
 * @return mixed
 *    entity object or null.
 */
function entity_ctools_entity_form_context_get_entity($entity_type, $entity_id) {
  $entity_info = entity_get_info($entity_type);
  if (is_numeric($entity_id)) {
    $entity = entity_load($entity_type, array($entity_id));
    $entity = $entity[$entity_id];
  }
  elseif (module_exists('uuid') && isset($entity_info['entity keys']['uuid']) && uuid_is_valid($entity_id)) {
    $entity = entity_uuid_load($entity_type, array($entity_id));
    $entity = reset($entity);
    entity_make_entity_local($entity_type, $entity);
  }
  return $entity;
}

/**
 * Ctools contexts callback: edit form.
 */
function entity_ctools_entity_form_context_settings_form($form, &$form_state) {
  $conf = &$form_state['conf'];
  $plugin = &$form_state['plugin'];
  list(, $entity_type) = explode(':', $plugin['name']);

  $types = array();
  $entity_info = entity_get_info($entity_type);

  foreach ($entity_info['bundles'] as $bundle_name => $bundle) {
    $types["{$entity_type}:{$bundle_name}"] = $bundle['label'];
  }

  $form['redirect'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect'),
    '#description' => t('You can use % as placeholder for the entity_id. Entity tokens will also be replaced.'),
    '#default_value' => $conf['redirect'],
  );

  $form['action'] = array(
    '#title' => t('Action'),
    '#type' => 'radios',
    '#options' => array(
      'add' => t('Add entity', array('@entity_type' => $entity_info['label'])),
      'edit' => t('Edit entity', array('@entity_type' => $entity_info['label'])),
    ),
    '#default_value' => $conf['action'],
    '#description' => t('Select the @entity_type bundle for this form.', array('@entity_type' => $entity_type)),
  );

  $form['type'] = array(
    '#title' => t('Bundle'),
    '#type' => 'select',
    '#options' => $types,
    '#default_value' => $conf['type'],
    '#description' => t('Select the @entity_type bundle for this form.', array('@entity_type' => $entity_type)),
    '#dependency' => array('radio:action' => array('add')),
  );

  $description = array();
  $description[] = t('Enter the title or entity ID.');
  $description[] = t('Use [id: 123] format to enter the entity id.');

  $form['entity'] = array(
    '#title' => t('Title / ID'),
    '#type' => 'textfield',
    '#description' => implode(' ', $description),
    '#maxlength' => 512,
    '#autocomplete_path' => 'ctools/autocomplete/' . $entity_type,
    '#dependency' => array('radio:action' => array('edit')),
  );

  if (!empty($conf['entity_id'])) {
    $entity = entity_ctools_entity_form_context_get_entity($entity_type, $conf['entity_id']);

    if ($entity) {
      $uri = entity_uri($entity_type, $entity);

      $options = array(
        'attributes' => array(
          'target' => '_blank',
          'title' => t('Open in new window'),
        ),
        'html' => TRUE,
      );
      $args = array(
        '%title' => $entity->{$entity_info['entity keys']['label']},
        '%type' => $entity_type,
        '%id' => $entity->{$entity_info['entity keys']['id']},
      );

      if ($entity_info['entity keys']['label']) {
        $text = t("'%title' [%type id %id]", $args);
      }
      else {
        $text = t("[%type id %id]", $args);
      }

      if (is_array($uri)) {
        $link = l($text, $uri['path'], $options);
      }
      elseif ($uri) {
        $link = l($text, file_create_url($uri), $options);
      }
      else {
        $link = $text;
      }

      $form['entity']['#description'] = t('Currently set to !link', array('!link' => $link)) . '<br/>' . $form['entity']['#description'];
    }
  }

  $form['entity_id'] = array(
    '#type' => 'value',
    '#value' => $conf['entity_id'],
  );

  return $form;
}

/**
 * Ctools contexts callback: edit form validator.
 */
function entity_ctools_entity_form_context_settings_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $conf = &$form_state['conf'];

  if ($values['action'] == 'edit') {

    if (empty($values['entity']) && isset($conf['entity_id'])) {
      return;
    }

    $plugin = &$form_state['plugin'];
    list($entity_type) = explode(':', $values['type']);
    $entity_info = entity_get_info($entity_type);

    $id = $values['entity'];
    $preg_matches = array();
    $match = preg_match('/id:\s?(\d+)/', $id, $preg_matches);

    if ($match) {
      $id = $preg_matches[1];
    }
    if (is_numeric($id)) {
      $entity = entity_load($entity_type, array($id));
      $entity = $entity[$id];
    }
    else {
      $field = $entity_info['entity keys']['label'];
      $entity = entity_load($entity_type, FALSE, array($field => $id));
    }

    if (!$entity) {
      form_error($form['entity'], t('Invalid entity selected.'));
    }
    else {
      if (isset($entity_info['entity keys']['uuid'])) {
        $id = $entity->{$entity_info['entity keys']['uuid']};
      }
      else {
        $id = $entity->{$entity_info['entity keys']['id']};
      }
      form_set_value($form['entity_id'], $id, $form_state);
    }
  }

}

/**
 * Ctools contexts callback: edit form submit handler.
 */
function entity_ctools_entity_form_context_settings_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $conf = &$form_state['conf'];

  $conf['redirect'] = $values['redirect'];
  $conf['action'] = $values['action'];

  if ($values['action'] == 'edit') {
    $form_state['conf']['entity_id'] = $form_state['values']['entity_id'];
    unset($conf['type']);
  }
  else {
    $conf['type'] = $values['type'];
    unset($form_state['conf']['entity_id']);
  }

}

/**
 * Convert a context into a string.
 */
function entity_ctools_entity_form_context_convert($context, $type) {
  list(, $entiy_type) = explode(':', $context->plugin);
  $entity_info = entity_get_info($entiy_type);
  $bundle = $entity_info['bundles'][$context->data->{$entity_info['entity keys']['bundle']}];

  switch ($type) {
    case 'type':
      return $entity_info['label'];

    case 'bundle':
      return $bundle['label'];
  }
}
