<?php

/**
 * @file
 * Ctools content_type plugin definition.
 */

$plugin = array(
  'single' => TRUE,
  'icon' => 'icon_form.png',
  'title' => t('Entity form: Submit buttons'),
  'description' => t('Submit buttons for the entity form.'),
  'required context' => new ctools_context_required(t('Form'), 'entity_form'),
  'category' => t('Form'),
);

/**
 * Content type render callback.
 */
function entity_ctools_entity_form_buttons_content_type_render($subtype, $conf, $panel_args, &$context) {
  $block = new stdClass();
  $block->module = t('entity_form');
  $block->title = '';
  $block->delta = 'buttons';

  if (isset($context->form)) {
    $block->content = array();
    foreach (array('actions', 'form_token', 'form_build_id', 'form_id') as $element) {
      $block->content[$element] = isset($context->form[$element]) ? $context->form[$element] : NULL;
      unset($context->form[$element]);
    }
  }
  else {
    $block->content = t('Entity form buttons.');
  }
  return $block;
}

/**
 * Content type admin title callback.
 */
function entity_ctools_entity_form_buttons_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" entity form submit buttons', array('@s' => $context->identifier));
}

/**
 * Content type settings form callback.
 */
function entity_ctools_entity_form_buttons_content_type_edit_form($form, &$form_state) {
  // Provide a blank form so we have a place to have context setting.
  return $form;
}
