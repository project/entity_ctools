<?php

/**
 * @file
 * Plugin to provide a relationship handler for entity form from entity.
 */

$plugin = array(
  'title' => t('Entity form from entity'),
  'keyword' => 'entity_form',
  'description' => t('Adds entity form from an entity context.'),
  'get child' => 'entity_ctools_entity_form_from_entity_get_child',
  'get children' => 'entity_ctools_entity_form_from_entity_get_children',
  'context' => 'entity_ctools_entity_form_context_from_entity',
  'edit form' => 'entity_ctools_entity_form_from_entity_settings_form',
  'defaults' => array('redirect' => FALSE),
);

/**
 * Ctools contexts callback: get child.
 */
function entity_ctools_entity_form_from_entity_get_child($plugin, $parent, $child) {
  $plugins = entity_ctools_entity_form_from_entity_get_children($plugin, $parent);
  return $plugins[$parent . ':' . $child];
}

/**
 * Ctools contexts callback: get children.
 */
function entity_ctools_entity_form_from_entity_get_children($plugin, $parent) {
  $plugins = entity_ctools_entity_form_get_children($plugin, $parent);

  foreach ($plugins as &$child_plugin) {
    $child_plugin['required context'] = new ctools_context_required(t('Entity'), 'entity:' . $child_plugin['entity type']);
  }
  return $plugins;
}

/**
 * Return a new context based on an existing context.
 */
function entity_ctools_entity_form_context_from_entity($context, $conf, $placeholders = FALSE) {

  if (empty($context->data)) {
    return ctools_context_create_empty($conf['name'], NULL);
  }

  if (isset($context->data)) {
    return ctools_context_create($conf['name'], $context->data, $conf);
  }

}

/**
 * Settings form for the relationship.
 */
function entity_ctools_entity_form_from_entity_settings_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['redirect'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect'),
    '#description' => t('You can use % as placeholder for the entity_id. Entity tokens will also be replaced.'),
    '#default_value' => $conf['redirect'],
  );
  return $form;
}
