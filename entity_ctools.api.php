<?php

/**
 * @file
 * Hooks provided by the Entity Ctools module.
 */

/**
 * Alter the definition of an entity form context plugin.
 *
 * @param array $plugin
 *   An associative array defining a plugin.
 * @param array $entity_info
 *   The entity info array of a specific entity type.
 * @param string $plugin_id
 *   The plugin ID, in the format NAME:KEY.
 */
function hook_entity_ctools_entity_form_context_alter(&$plugin, $entity_info, $plugin_id) {
  ctools_include('context');
  switch ($plugin_id) {
    case 'entity_id:taxonomy_term':
        $plugin['no ui'] = TRUE;
    case 'entity:user':
        $plugin = ctools_get_context('user');
      unset($plugin['no ui']);
      unset($plugin['no required context ui']);

      break;
  }
}

/**
 * Alter the definition of entity form context plugins.
 *
 * @param array $plugins
 *   An associative array of plugin definitions, keyed by plugin ID.
 *
 * @see hook_entity_ctools_entity_form_context_alter()
 */
function hook_entity_ctools_entity_form_contexts_alter(&$plugins) {
  $plugins['entity_id:taxonomy_term']['no ui'] = TRUE;
}

/**
 * Define form information to provide support for additional entity types.
 */
function hook_entity_ctools_entity_form_info($plugin, $parent) {

  return array(
    'commerce_order' => array(
      'module' => 'commerce_order',
      'file' => 'includes/commerce_order.forms',
      'form_id' => 'commerce_order_order_form',
      'entity_values' => array(
        'status' => 'pending',
      ),
    ),
    'commerce_product' => array(
      'module' => 'commerce_product',
      'file' => 'includes/commerce_product.forms',
      'form_id' => 'commerce_product_product_form',
    ),
    'crm_core_contact' => array(
      'module' => 'crm_core_contact_ui',
      'file' => 'crm_core_contact_ui.pages',
      'form_id' => 'crm_core_contact_ui_form',
    ),
    'crm_core_activity' => array(
      'module' => 'crm_core_activity_ui',
      'file' => 'crm_core_activity_ui.pages',
      'form_id' => 'crm_core_activity_form',
    ),
  );

}

/**
 * Alter the form information for entity types.
 */
function hook_entity_ctools_entity_form_info_alter(&$form_info, $plugin, $parent) {

  $form_info['commerce_order'] = array(
    'module' => 'commerce_order',
    'file' => 'includes/commerce_order.forms',
    'form_id' => 'commerce_order_order_form',
    'entity_values' => array(
      'status' => 'pending',
    ),
  );

}
